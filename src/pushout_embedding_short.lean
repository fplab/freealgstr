import .pushout_paths
import hott.types.sigma

universes u v w

hott_theory
namespace hott
namespace pushout

open hott hott.eq hott.is_equiv hott.is_trunc hott.function hott.sigma hott.sum
local attribute [instance] is_prop_fiber_of_is_embedding

section
parameters {A : Type u} {B : Type v} {C : Type w} (f : A → B) (g : A → C) [is_embedding f]
include f g

@[hott] def motive {c₀} : Π x (q : (inr c₀ : pushout f g) = quotient.class_of _ x), Type _
| (sum.inl b) q := Σ (a : fiber f b), fiber (ap inr) (q ⬝ ap inl a.2⁻¹ ⬝ (glue a.1))
| (sum.inr c) q := fiber (ap inr) q

@[hott] def fib_rec (c₀ c q) : @motive c₀ (sum.inr c) q :=
let Qcons : Π a (p : inr c₀ = inl (f a)), motive (sum.inl (f a)) p ≃ motive (sum.inr (g a)) (p ⬝ glue a) :=
 λ a p, @sigma_equiv_of_is_contr_left _
    (λ (a : fiber f (f a)), fiber (ap inr) (p ⬝ ap inl (a.2)⁻¹ ⬝ glue (a.1))) 
    (is_contr_of_inhabited_prop ⟨a, idp⟩) in
pushout.path_rec f g _ motive ⟨idp, idp⟩ Qcons _ q

@[hott] protected def preserves_embedding : is_embedding inr :=
λ c c', adjointify _ (λ q, (fib_rec c c' q).1) (λ p, (fib_rec c c' p).2) (λ p, by { hinduction p, refl })

end

end pushout
end hott