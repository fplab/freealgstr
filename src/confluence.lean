import .trc .list_repl .wf

open hott hott.trunc hott.is_equiv hott.quotient hott.equiv hott.eq
open hott.is_trunc hott.pi
open hott.trc hott.list_repl hott.acc

universes u v w w'

hott_theory

namespace hott
section

variables {A : Type u} (R : A → A → Type (max u v))

local notation `R*` := trc R
local notation c`::`r := trc.snoc c r
local notation c`:::`:100d := trc.trans _ c d
local notation `Rs*` := trc (trc.sc R)
local notation r`⁺`:1200 := trc.sc.pos r
local notation r`⁻`:1200 := trc.sc.neg r
local notation `L ` := tc (list_repl (λ a b, tc R b a))

@[hott] def trc_of_tc {a b : A} (r : tc R a b) : trc R a b :=
by { hinduction r with a b r a b c r s ih, exact trc.base _ r, exact ih :: s }

@[hott] def tc_of_trc_cons {a b c : A} (r : trc R a b) (s : R b c) : tc R a c :=
begin
  revert c s, hinduction r with a a b c r s ih,
  { intros, fconstructor, assumption },
  { intros c' s', exact tc.snoc (ih s) s' }
end

@[hott] def tc_of_trc_snoc {a b c : A} (r : R a b) (s : trc R b c) : tc R a c :=
begin
  revert a r, hinduction s; intros, fconstructor, assumption,
  exact tc.snoc (ih r) a_2, 
end

@[hott] def tc_or_refl_of_trc {a b : A} (r : R* a b) :
  (trc_length _ r = 0)
  ⊎ tc R a b :=
begin
  hinduction r,
  { apply sum.inl, refl },
  { clear ih, apply sum.inr, apply tc_of_trc_cons, assumption, assumption }
end

@[hott] def tc_list_repl_pair {a b : A} :
  tc (list_repl R) [] [a, b] :=
begin
  have H1 : tc (list_repl R) [] [a],
  { fconstructor, apply list_repl_of_list_rel, fconstructor },
  have H2 : tc (list_repl R) [a] [a, b],
  { fconstructor, apply list_repl.replace b [] [] [a], fconstructor },
  exact tc.trans _ H1 H2,
end

@[hott] def acc_trc_span_filter {a : A} (ac : acc (λ a b, R b a) a) (r : Rs* a a) :
  (trc.trc_length _ r = 0) ⊎ (trc.has_span R r) :=
begin
  hinduction trc.span_filter R r with eHs Hs Hi; clear eHs,
  { apply sum.inr, assumption },
  hinduction Hi with Hi Hd; apply sum.inl,
  { hinduction tc_or_refl_of_trc R (trc_of_increasing R r Hi) with m H H; clear m,
    { rwr length_trc_of_increasing at H, assumption },
    { apply empty.elim, apply acc_irreflexive (acc_tc ac), apply tc_swap, exact H } },
  { hinduction tc_or_refl_of_trc R (trc_of_decreasing R r Hd) with m H H; clear m,
    { rwr legnth_trc_of_decreasing at H, assumption },
    { apply empty.elim, apply acc_irreflexive (acc_tc ac), apply tc_swap, exact H } }
end

-- CvW Definition 17
@[hott] def is_locally_confluent : Type _ :=
∀ a b c, R a b → R a c → Σ (d : A), R* b d × R* c d

-- This section contains construction on _one_ confluence square
section
   variables (C : is_locally_confluent R) {a b c : A} (r : R a b) (s : R a c)

@[hott] def conf_point : A :=
(C a b c r s).1

@[hott] def conf_necklace : Rs* b c := 
(trsc_of_trc_pos R (C a b c r s).2.1) ::: (trsc_of_trc_neg R (C a b c r s).2.2)

@[hott] def conf_cycle : Rs* b b :=
((conf_necklace R C r s) :: s⁻) :: r⁺

@[hott] def left_leg_list_rel (r : R a b) {d : A} (p : R* b d) :
  list_rel (λ a b, tc R b a) (rvertices _ (trsc_of_trc_pos R p)) a :=
begin
  hinduction p, fconstructor,
  fconstructor, apply ih, assumption,
  exact tc.trans _ (tc.base r) (tc_of_trc_cons R a_2 a_3),
end

@[hott] def right_leg_list_rel (s : R a c) {d : A} (q : R* c d) :
  list_rel (λ a b, tc R b a) (rvertices _ (trsc_of_trc_neg R q)) a :=
begin
  hinduction q, fconstructor,
  change list_rel _ (rvertices _ (_ ::: (trsc_of_trc_neg R a_2))) _,
  rwr rvertices_append, apply list_rel_append, apply ih s,
  change list_rel _ [_] _,
  fconstructor, fconstructor, apply tc_of_trc_snoc, exact s, assumption
end

@[hott] def conf_necklace_list_rel :
  list_rel (λ a b, tc R b a) (rvertices _ (conf_necklace R C r s)) a :=
begin
  dsimp[conf_necklace],
  hinduction (C a b c r s) with mem d C'; hinduction C' with p q; dsimp,
  rwr [rvertices_append], apply list_rel_append,
  { apply right_leg_list_rel, assumption },
  { apply left_leg_list_rel, assumption }
end

@[hott] def conf_necklace_list_repl :
  L (rvertices _ (conf_necklace R C r s)) [c, a] :=
begin
  hgeneralize ep : conf_necklace R C r s = p,
  hinduction p with b b c d r s ih,
  { apply tc_list_repl_pair },
  { fconstructor, apply list_repl_pair_of_list_rel,
    change list_rel _ (rvertices _ (r :: s)) a, rwr ←ep,
    apply conf_necklace_list_rel }
end

end

@[hott] def tc_list_repl_append_left {xs ys zs : list A}
  (r : tc (list_repl R) xs ys) : tc (list_repl R) (zs ++ xs) (zs ++ ys) :=
begin
  hinduction r,
  { fconstructor, apply list_repl_append_left, assumption },
  { apply tc.snoc, exact ih, apply list_repl_append_left, assumption }
end

@[hott] def tc_list_repl_append_right {xs ys zs : list A}
  (r : tc (list_repl R) xs ys) : tc (list_repl R) (xs ++ zs) (ys ++ zs) :=
begin
  hinduction r,
  { fconstructor, apply list_repl_append_right, assumption },
  { apply tc.snoc, exact ih, apply list_repl_append_right, assumption }
end

-- CvW Lemma 43
@[hott] def necklace_decomp' (C : is_locally_confluent R) {x : A}
  (acx : acc (λ a b, R b a) x) (p : Rs* x x) :
  (trc_length _ p = 0)
  ⊎ (Σ (a b c : A) (s : R b a) (t : R b c) (p1 : Rs* x a) (p2 : Rs* c x)
       (ep : p = ((p1 :: s⁻) :: t⁺) ::: p2),
       L (rvertices _ (p1 ::: (conf_necklace R C s t) ::: p2)) (rvertices _ p))
  ⊎ (Σ (b c : A) (s : R x b) (t : R x c) (p' : Rs* c b)
       (ep : p = (trc.base _ t⁺) ::: p' :: s⁻),
       L (rvertices _ (p' ::: (conf_necklace R C s t))) (rvertices _ p)) :=
begin
  have := acc_trc_span_filter R acx p, hinduction this with fo b ar,
  { apply sum.inl, assumption },
  { apply sum.inr, hinduction b,
    { apply sum.inl,
      have : L (rvertices (trc.sc R) (c ::: conf_necklace R C r s ::: d))
               (rvertices (trc.sc R) (((c :: r⁻) :: s⁺):::d)),
      { rwr [rvertices_append,rvertices_append,rvertices_append,←list.append_assoc],
        apply tc_list_repl_append_left,
        change L _ (rvertices _ ((_ ::: trc.base _ r⁻) :: _)),
        rwr [trc.trc_snoc_assoc,rvertices_append],
        apply tc_list_repl_append_right,
        apply conf_necklace_list_repl R C r s },
      exact ⟨_, _, _, r, s, c, d, idp, this⟩ },
    { apply sum.inr,
      have H1 : L (rvertices (trc.sc R) (c ::: conf_necklace R C s r))
                  (rvertices (trc.sc R) ((trc.base _ r⁺) ::: c :: s⁻)),
      { fapply tc.trans, exact rvertices _ (c :: s⁻),
        change L _ (rvertices _ (_ ::: trc.base _ _)),
        rwr [rvertices_append, rvertices_append], apply tc_list_repl_append_right,
        fconstructor, apply list_repl_of_list_rel, apply conf_necklace_list_rel,
        fconstructor,
        rwr rvertices_append,
        have lp' : list_rel (λ a b, tc R b a) [] x_1, fconstructor,
        have lp := list_repl.replace x_1 [] [] (rvertices _ (c :: s⁻)) lp',
        rwr [hott.list.append_nil] at lp, change list_repl _ (_ ++ []) _ at lp,
        rwr [hott.list.append_nil] at lp, exact lp },
      exact ⟨_, _, s, r, c, idp, H1⟩ } }
end

-- CvW Theorem 46
@[hott] def cycle_induction
  (C : is_locally_confluent R)
  (Q : Π x, Rs* x x → Type w)
  (Qnil : Π x (p : Rs* x x), trc_length _ p = 0 →  Q x p)
  (Qconf : Π a b c (s : R b a) (t : R b c),
     Q a (conf_cycle R C s t))
  (Qmerge : Π {x a b} (p1 : Rs* x a) (p2 : Rs* b x) (n q : Rs* a b),
     Q x (p1 ::: n ::: p2) → Q a (n ::: (trsc_inv _ q)) → Q x (p1 ::: q ::: p2))
  (Qrot : Π {a b} (s : (trc.sc R) a b) (p : Rs* b a),
     Q b (p :: s) → Q a ((trc.base _ s) ::: p)) :
  Π x p, Q x p :=
begin
  intros x p, hgeneralize evp : rvertices _ p = vp,
  have wfL : well_founded L,
  { fconstructor, intro vp, apply acc_tc, apply acc_list_repl, 
    apply acc.wf.functor (tc (λ a b, R b a)) (λ a b, tc_of_tc_swap _),
    apply wf_tc_of_wf, assumption },
  revert x p evp, apply wf.elim wfL vp,
  clear vp, intros vp H x p evp,
  hinduction necklace_decomp' R C (wf_acc N x) p with eH H H'; clear eH,
  { apply Qnil, assumption },
  hinduction H' with H H,
  { hinduction H with a H, hinduction H with b H, hinduction H with c H,
    hinduction H with s H, hinduction H with t H, hinduction H with p1 H,
    hinduction H with p2 H, hinduction H with ep lp,
    rwr ep, rwr ←evp at H, have Qn := H _ lp _ _ idp,
    change Q x (((_ ::: trc.base _ _) :: _) ::: p2),
    rwr [trc.trc_snoc_assoc, trc.trc_assoc],
    apply Qmerge p1 p2 (conf_necklace R C s t) (trc.base (trc.sc R) s⁻ :: t⁺) Qn,
    apply Qconf },
  { hinduction H with b H, hinduction H with c H, hinduction H with s H,
    hinduction H with t H, hinduction H with p' H, hinduction H with ep lp,
    rwr ep, rwr ←evp at H, clear evp vp,
    change Q x ((_ ::: _) ::: trc.base _ _), rwr trc.trc_assoc,
    apply Qrot, rwr trc.trc_snoc_assoc,
    apply Qmerge p' (trc.refl _) (conf_necklace R C s t),
    { apply H _ lp c (p':::conf_necklace R C s t) idp },
    { apply Qconf } }
end

end

end hott