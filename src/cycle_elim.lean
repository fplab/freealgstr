import .util .confluence .trc .trsc_glue

universes u v w

hott_theory

open hott hott.trunc hott.function hott.is_equiv hott.quotient hott.sigma hott.equiv hott.eq
open hott.is_trunc hott.pi hott.trc

namespace hott

section
  parameters {A : Type u} {R : A → A → Type (max u v)}

local notation `D ` := quotient R
local notation `ι ` := class_of R
local notation c`::`r := trc.snoc c r
local notation c`:::`:100d := trc.trans _ c d
local notation `R* ` := trc R
local notation `Rs* ` := trc (trc.sc R)


-- CvW Lemma 13
@[hott] def quotient_prop_cycle (P : Π (x : D), x = x → Type w)
  [∀ x q, is_prop (P x q)] :
  (Π (a : A) (r : Rs* a a), P _ (trsc_glue r))
  ≃ (Π (x : D) (q : x = x), P x q) :=
begin
  apply is_trunc.equiv_of_is_prop,
  { intros H x q, hinduction x,
    { hinduction tcsc_glue_surj a a q with e f,
      hinduction f with r er, rwr ←er, exact H a r },
    { dsimp, apply is_prop.elimo } },
  { intros H a r, exact H (ι a) (trsc_glue r) }
end

-- CvW Theorem 14
@[hott] def quotient_1type_char (X : Type u) [is_trunc 1 X] :
  (trunc 0 D → X)
  ≃ (Σ (f : A → X) (h : ∀ a b, R a b → f a = f b),
      ∀ a (s : Rs* a a), trsc_eq_lift R f h s = idp) :=
begin
  transitivity, apply one_trunc_fn_factor',
  transitivity, apply sigma_equiv_sigma_right, intro k, symmetry,
    apply quotient_prop_cycle (λ a p, ap k p = idp),
  transitivity, apply sigma_equiv_sigma_right, intro k,
    transitivity, apply pi_equiv_pi_right, intro a,
    transitivity, apply pi_equiv_pi_right, intro r, dsimp[trsc_glue],
    rwr trsc_ap_compose R (class_of R) (eq_of_rel R) k a a r, dsimp,
    refl, refl, -- why do we need those extra `transitivity`s here?
  fconstructor,
  { intro H, fconstructor, intro a, exact H.1 (ι a),
    fconstructor, intros a b r, apply ap H.fst, apply eq_of_rel _ r,
    intros a r, apply H.2 _ },
  fapply adjointify,
  { intros H, fconstructor, intro x, hinduction x, exact H.1 a,
    apply H.2.1, assumption, intros a r, dsimp,
    have : (λ a b (r : R a b), ap (quotient.elim H.1 H.2.1) (eq_of_rel R r))
      = H.2.1,
    { fapply eq_of_homotopy, intro a, apply eq_of_homotopy, intro b,
      fapply eq_of_homotopy, intro r, apply elim_eq_of_rel },
    rwr this, apply H.2.2 a r },
  { intro H, dsimp, hinduction H with f H, hinduction H with h H,
    dsimp, apply sigma_eq_right, fapply dpair_eq_dpair,
    fapply eq_of_homotopy, intro a, apply eq_of_homotopy, intro b,
    fapply eq_of_homotopy, intro r, apply elim_eq_of_rel,
    apply is_prop.elimo },
  { intro H, dsimp, hinduction H with f H, dsimp, fapply dpair_eq_dpair, 
    { apply quotient.eta },
    { apply is_prop.elimo }  }
end

-- CvW Theorem 47
@[hott] def conf_quotient_1type_char (X : Type u) [is_trunc 1 X] 
  (C : is_locally_confluent R)
  (wf : acc.well_founded (λ a b, R b a)) :
  (trunc 0 D → X)
  ≃ (Σ (f : A → X) (h : ∀ a b, R a b → f a = f b),
      (∀ a (p : a = a), ap f p = idp) 
      × (∀ a b c (s : R a b) (t : R a c), 
          trsc_eq_lift R f h (conf_cycle R C s t) = idp)) :=
begin
  transitivity, apply quotient_1type_char,
  apply sigma_equiv_sigma_right, intro f,
  apply sigma_equiv_sigma_right, intro h,
  apply equiv_of_is_prop,
  { intro H, fconstructor, 
    { intros a p, have e := H a (trc_of_eq _ p), rwr trsc_eq_lift_of_trc_of_eq at e,
      exact e },
    { intros a b c s t, apply H } },
  { intros H a r, 
    apply cycle_induction R C wf (λ a r, trsc_eq_lift R f h r = idp); clear r a,
    { intros a r e, have := (trc_zero_length R r e).2,
      rwr this, clear this, rwr trsc_eq_lift_of_trc_of_eq, apply H.1 },
    { intros a b c s t, apply H.2 },
    { intros x a b p1 p2 n q e1 e2,
      rwr [trsc_eq_lift_append, trsc_eq_lift_append] at e1,
      rwr [trsc_eq_lift_append, trsc_eq_lift_inv] at e2,
      rwr [trsc_eq_lift_append, trsc_eq_lift_append, ←eq_of_con_inv_eq_idp e2],
      exact e1 },
    { intros a b s p e, rwr [trsc_eq_lift_append],
      change trsc_eq_lift _ _ _ (p ::: trc.base _ _) = _ at e,
      rwr trsc_eq_lift_append at e, rwr [eq_inv_of_con_eq_idp e, con.left_inv] } }
end

end

end hott