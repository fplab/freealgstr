import hott.algebra.relation hott.types.sigma hott.types.pi .util

open hott hott.trunc hott.is_equiv hott.quotient hott.equiv hott.eq
open hott.is_trunc hott.pi hott.relation

universes u v w w'

hott_theory

namespace hott
  variables {A : Type u} (R : A → A → Type (max u v))

@[hott] inductive sc : A → A → Type (max u v)
| pos : ∀ {a b}, R a b → sc a b
| neg : ∀ {a b}, R b a → sc a b

@[hott] inductive tc : A → A → Type (max u v)
| base  : ∀ {a b}, R a b → tc a b
| snoc : ∀ {a b c}, tc a b → R b c → tc a c

@[hott] def tc.trans : ∀ {a b c}, tc R a b → tc R b c → tc R a c :=
begin
  intros a b c r s, hinduction s with _ _ s _ _ _ _ s, exact tc.snoc r s,
  exact tc.snoc (ih r) s,
end

local notation c`::`r := tc.snoc c r
local notation c`:::`:100d := tc.trans _ c d

@[hott] def trans_snoc_assoc {a b c d} (p : tc R a b) (q : tc R b c) (r : R c d) :
  (p ::: q) :: r = p ::: (q :: r) :=
by { hinduction q with r, refl, refl }

@[hott] def tc_assoc {a b c d} (p : tc R a b) (q : tc R b c) (r : tc R c d) :
  ((p ::: q) ::: r) = p ::: (q ::: r) :=
begin
  hinduction r, apply trans_snoc_assoc,
  have := ih q, rwr ←trans_snoc_assoc, rwr this
end

@[hott] def trans_base_eq_snoc {a b c} (p : tc R a b) (r : R b c) :
  (p ::: (tc.base r)) = (p :: r) :=
rfl

local notation r`⁺`:1200 := sc.pos r
local notation r`⁻`:1200 := sc.neg r

@[hott] inductive has_span {a b : A} : tc (sc R) a b → Type (max u v)
| inner : ∀ {x y z} (c : tc (sc R) a x) (d : tc (sc R) z b)
            (r : R y x) (s : R y z), has_span (((c :: r⁻) :: s⁺) ::: d)
| outer : ∀ {x y} (c : tc (sc R) x y) (r : R a x) (s : R b y),
                  has_span (tc.base r⁺ ::: c :: s⁻)
| left : ∀ {x y} (r : R x a) (s : R x y) (c : tc (sc R) y b),
           has_span ((tc.base r⁻ :: s⁺) ::: c)
| right : ∀ {x y} (c : tc (sc R) a x) (r : R y x) (s : R y b),
            has_span ((c :: r⁻) :: s⁺)
| base : ∀ {x} (r : R x a) (s : R x b), has_span (tc.base r⁻ :: s⁺)
| cospan : ∀ {x} (r : R a x) (s : R b x), has_span (tc.base r⁺ :: s⁻)

@[hott] inductive is_increasing : Π {a b : A}, (tc (sc R) a b) → Type (max u v)
| base : ∀ a b (r : R a b), is_increasing (tc.base r⁺)
| snoc : ∀ a b c (d : tc (sc R) a b) (s : R b c),
           is_increasing d → is_increasing (d :: s⁺)

@[hott] inductive is_decreasing : Π {a b : A}, (tc (sc R) a b) → Type (max u v)
| base : ∀ a b (r : R a b), is_decreasing (tc.base r⁻)
| snoc : ∀ a b c (d : tc (sc R) a b) (s : R c b),
           is_decreasing d → is_decreasing (d :: s⁻)

@[hott] def has_span_snoc {a b c : A} (d : tc (sc R) a b) (r : (sc R) b c) :
  has_span R d → has_span R (d :: r) :=
begin
  intro H, hinduction H,
  { rwr trans_snoc_assoc, fconstructor },
  { hinduction r with _ _ r _ _ r,
    { fconstructor },
    { rwr trans_snoc_assoc, fconstructor } },
  { rwr trans_snoc_assoc, fconstructor },
  { rwr ←trans_base_eq_snoc, fconstructor },
  { rwr ←trans_base_eq_snoc, fconstructor },
  { hinduction r with _ _ r _ _ r,
    { fconstructor },
    { change has_span R ((_ ::: tc.base s⁻) :: r⁻),
      rwr trans_snoc_assoc, fconstructor } }
end

@[hott] def increasing_tail {a b c : A} (r : tc (sc R) a b) (H : is_increasing R r)
  (s : tc (sc R) b c)
   : Σ (b' : A) (t : R a b') d', (r ::: s) = tc.base t⁺ ::: d' :=
begin
  hinduction H, exact ⟨b, r, s, rfl⟩,
  hinduction ih (tc.base s⁺ ::: s_1) with s b' H,
  hinduction H with t H, hinduction H with d' e,
  exact ⟨b', t, d', by{ rwr ←trans_base_eq_snoc, rwr tc_assoc, exact e }⟩
end

@[hott] def span_of_increasing_snoc {a b c : A} (r : tc (sc R) a b) (s : R c b)
  (H : is_increasing R r) : has_span R (r :: s⁻) :=
begin
  hinduction H,
  { fconstructor },
  { have tl := increasing_tail R d a_1 (tc.base s⁺), 
    change has_span R ((d ::: tc.base s⁺):: _),
    hinduction tl with b' tl, hinduction tl with t tl,
    hinduction tl with d' e, rwr e, fconstructor }
end

@[hott] def span_of_decreasing_snoc {a b c : A} (r : tc (sc R) a b) (s : R b c)
  (H : is_decreasing R r) : has_span R (r :: s⁺) :=
by hinduction H; fconstructor

@[hott] def span_filter {a b : A} (c : tc (sc R) a b) :
  (has_span R c) ⊎ (is_increasing R c) ⊎ (is_decreasing R c):=
begin
  hinduction c with a b s a b c r s IH,
  { hinduction s with s,
    { apply sum.inr, apply sum.inl, fconstructor },
    { apply sum.inr, apply sum.inr, fconstructor } },
  { hinduction IH with H IH,
    { apply sum.inl, apply has_span_snoc, apply H },
    { hinduction IH with H H,
      { hinduction s with _ _ s _ _ s,
        { apply sum.inr, apply sum.inl, fconstructor, assumption },
        { apply sum.inl, apply span_of_increasing_snoc, assumption } },
      { hinduction s with _ _ s _ _ s,
        { apply sum.inl, apply span_of_decreasing_snoc, assumption },
        { apply sum.inr, apply sum.inr, fconstructor, assumption } } } }
end

@[hott] def tc_of_increasing {a b : A} (r : tc (sc R) a b) (H : is_increasing R r) :
  tc R a b :=
by { hinduction H, exact tc.base r, exact ih :: s }

@[hott] def tc_of_decreasing {a b : A} (r : tc (sc R) a b) (H : is_decreasing R r) :
  tc R b a :=
by { hinduction H, exact tc.base r, exact tc.base s ::: ih }

@[hott] def tcsc_eq_lift {A' : Type w} (f : A → A')
  (h : Π {a b}, R a b → f a = f b) {a b : A} (r : tc (sc R) a b) : f a = f b :=
begin
  hinduction r,
  { hinduction a_1, apply h, assumption, symmetry, apply h, assumption },
  { refine ih ⬝ _, hinduction a_2,
    { apply h, assumption },
    { symmetry, apply h, assumption } }
end

@[hott] def tcsc_ap_compose {X : Type w} {Y : Type w'} (f : A → X)
  (h : Π {a b}, R a b → f a = f b) (k : X → Y) (a b) (r : tc (sc R) a b):
  ap k (@tcsc_eq_lift A R X f @h a b r)
  = @tcsc_eq_lift A R Y (k ∘ f) (λ a b r, ap k (h r)) a b r :=
begin
  hinduction r with a b s a b c r s,
  { hinduction s with a b s, refl, apply ap_inv },
  { transitivity, apply ap_con, hinduction s with a b s,
    { apply hott.eq.whisker_right, exact ih },
    { change ap k (tcsc_eq_lift R f @h r) ⬝ _ = _,
      rwr ih, apply hott.eq.whisker_left, dsimp, apply ap_inv } }
end

@[hott] def tcsc_vertices {a b : A} (r : tc (sc R) a b) : list A :=
begin
  hinduction r,
  { hinduction a_1, exact [b, a], exact [a, b] },
  { hinduction a_2, exact list.cons b (list.cons a ih),
    exact list.cons a (list.cons b ih) }
end

@[hott] def tc_swap {a b} (r : (tc R) b a) : tc (λ a b, R b a) a b :=
begin
  hinduction r, fconstructor, assumption,
  fapply tc.trans, exact b, fconstructor, assumption, assumption
end

@[hott] def tc_of_tc_swap {a b} (r : tc (λ a b, R b a) a b) : tc R b a :=
begin
  hinduction r, fconstructor, assumption,
  fapply tc.trans, exact b, fconstructor, assumption, assumption
end

end hott