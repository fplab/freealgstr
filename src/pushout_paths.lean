import hott.hit.pushout
import .quotient_paths

universes u v w w'

hott_theory
namespace hott

namespace pushout

open hott hott.pushout hott.equiv hott.is_equiv hott.eq hott.quotient

section
  parameters {A : Type u} {B  : Type v} {C : Type w}
    (f : A → B) (g : A → C)

local notation `D ` := pushout f g
local notation `ι ` := (quotient.class_of _ : B ⊎ C → D)

section
  parameters (x : B ⊎ C)
    (Q : B ⊎ C → Type w')
    (Qrefl : Q x)
    (Qcons : Π (a : A), Q (sum.inl (f a)) ≃ Q  (sum.inr (g a)))
  include Qrefl Qcons

@[hott] private def prel : B ⊎ C → B ⊎ C → Type _ := 
@hott.pushout.pushout_rel A B C f g

@[hott] private def Qcons' (y z : B ⊎ C)  (r : prel y z) : Q y ≃ Q z :=
by { hinduction r with a, apply Qcons a }

@[hott] protected def path_elim (y : B ⊎ C) (p : ι x = ι y) : Q y :=
begin
  refine quotient.path_elim x Qrefl _ y p,
  exact Qcons' f g x Q Qrefl Qcons,
end

@[hott] protected def path_elim_refl : path_elim x idp = Qrefl :=
by { apply quotient.path_elim_refl }

@[hott] protected def path_elim_cons (a : A) (p : ι x = inl (f a))
  : path_elim (sum.inr (g a)) (p ⬝ (glue a))
    = Qcons a (path_elim (sum.inl (f a)) p) :=
begin
  apply quotient.path_elim_cons
end

end

section
  parameters (x : B ⊎ C)
    (Q : Π (y : B ⊎ C), ι x = ι y →  Type w')
    (Qrefl : Q x idp)
    (Qcons : Π (a : A) (p : ι x = inl (f a)), 
      Q (sum.inl (f a)) p ≃ Q (sum.inr (g a)) (p ⬝ glue a))
  include Qrefl Qcons

@[hott] private def prel : B ⊎ C → B ⊎ C → Type _ := 
@hott.pushout.pushout_rel A B C f g

@[hott] private def Qcons' (y z : B ⊎ C) (p : ι x = ι y) (r)
  : Q y p ≃ Q z (p ⬝ eq_of_rel prel r) :=
by { hinduction r with a; apply Qcons }

@[hott] protected def path_rec (y : B ⊎ C) (p : ι x = ι y) : Q y p :=
begin
  refine quotient.path_rec x Qrefl _ y p,
  exact Qcons' f g x Q Qrefl Qcons,
end

@[hott] protected def path_rec_refl : path_rec x idp = Qrefl :=
begin
  apply quotient.path_rec_refl
end

@[hott] protected def path_rec_cons (a : A) (p : ι x = inl (f a))
  : path_rec _ (p ⬝ glue a) = Qcons a p (path_rec _ p) :=
begin
  apply quotient.path_rec_cons
end

end

end

end pushout

end hott