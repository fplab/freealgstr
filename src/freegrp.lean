import .util .confluence .trsc_glue

open hott hott.trunc hott.is_equiv hott.quotient hott.equiv hott.eq
open hott.is_trunc hott.pi
open hott.trc hott.list_repl hott.acc hott.sum

universes u v w w'

hott_theory

namespace hott
section
  variables {A : Type u}

@[hott] def list_trisection {xs ys xs' ys' : list A}
  (p : xs ++ ys = xs' ++ ys') :
  (Σ zs, xs = xs' ++ zs × ys' = zs ++ ys)
  ⊎ (Σ zs, xs' = xs ++ zs × ys = zs ++ ys') :=
begin
  revert xs', hinduction xs with x xs ih; intros xs' p,
  { apply sum.inr, fconstructor, exact xs', fconstructor, refl, exact p },
  { hinduction xs' with x' xs' ih',
    { apply sum.inl, fconstructor, exact x :: xs, fconstructor, refl,
      symmetry, exact p },
    { change _ :: (_ ++ _) = _ :: (_ ++ _) at p,
      hinduction ih (list.cons_inj p).2 with mem H H; clear mem,
      { apply sum.inl, hinduction H with zs H, hinduction H with p1 p2,
        fconstructor, exact zs, fconstructor, rwr [p1, (list.cons_inj p).1],
        assumption },
      { apply sum.inr, hinduction H with zs H, hinduction H with p1 p2,
        fconstructor, exact zs, fconstructor, rwr [p1, (list.cons_inj p).1],
        assumption } } }
end

@[hott] def list_trisection_cons {xs ys xs' ys' : list A} {w w' : A}
  (p : xs ++ w :: ys = xs' ++ w' :: ys') :
  (Σ zs, xs = xs' ++ w' :: zs × ys' = zs ++ w :: ys)
  ⊎ (xs = xs' × w = w' × ys = ys')
  ⊎ (Σ zs, xs' = xs ++ w :: zs × ys = zs ++ w' :: ys') :=
begin
  hinduction list_trisection p with mem H H; clear mem;
    hinduction H with zs H; hinduction H with p1 p2; hinduction zs with z zs ih,
  { rwr list.append_nil at p1, change _ :: _ = _ :: _ at p2,
    apply sum.inr, apply sum.inl,
    fconstructor, exact p1, exact list.cons_inj p2⁻¹ᵖ },
  { apply sum.inl, rwr [p1, ←list.append_assoc] at p,
    hinduction list.cons_inj (list.cancel_left xs' p) with mem ez eys'; clear mem,
    fconstructor, exact zs, fconstructor, rwr ←ez, exact p1, exact eys'⁻¹ᵖ },
  { apply sum.inr, apply sum.inl,
    rwr list.append_nil at p1, change _ :: _ = _ :: _ at p2,
    fconstructor, exact p1⁻¹ᵖ, exact list.cons_inj p2 },
  { apply sum.inr, apply sum.inr, rwr [p1, ←list.append_assoc] at p,
    hinduction list.cons_inj (list.cancel_left xs p) with mem ez' eys; clear mem,
    fconstructor, exact zs, fconstructor, rwr ez', exact p1, exact eys }
end

@[hott] def list_trisection_cons_cons {xs ys xs' ys' : list A} {v w v' w' : A}
  (p : xs ++ v :: w :: ys = xs' ++ v' :: w' :: ys') :
    (Σ zs, xs = xs' ++ v' :: w' :: zs × ys' = zs ++ v :: w :: ys)
  ⊎ (xs = xs' ++ [v'] × v = w' × ys' = w :: ys)
  ⊎ (xs = xs' × v = v' × w = w' × ys = ys')
  ⊎ (xs' = xs ++ [v] × v' = w × ys = w' :: ys')
  ⊎ (Σ zs, xs' = xs ++ v :: w :: zs × ys = zs ++ v' :: w' :: ys') :=
begin
  hinduction list_trisection_cons p with mem H H; clear mem,
  { hinduction H with zs H, hinduction H with p1 p2,
    hinduction zs with z zs ih,
    { apply sum.inr, apply sum.inl, fconstructor, exact p1,
      have p' := list.cons_inj p2, exact ⟨p'.1⁻¹, p'.2⟩ },
    { apply sum.inl, fconstructor, exact zs, fconstructor,
      rwr (list.cons_inj p2).1, exact p1, exact (list.cons_inj p2).2 } },
  hinduction H with H H,
  { apply sum.inr, apply sum.inr, apply sum.inl,
    exact ⟨H.1, H.2.1, list.cons_inj H.2.2⟩ },
  { hinduction H with zs H, hinduction H with p1 p2,
    hinduction zs with z zs ih,
    { apply sum.inr, apply sum.inr, apply sum.inr, apply sum.inl,
      fconstructor, exact p1, have p' := list.cons_inj p2, exact ⟨p'.1⁻¹, p'.2⟩ },
    { apply sum.inr, apply sum.inr, apply sum.inr, apply sum.inr,
      fconstructor, exact zs, fconstructor, rwr (list.cons_inj p2).1, exact p1,
      exact (list.cons_inj p2).2 } }
end

@[hott] def flip : A ⊎ A → A ⊎ A :=
by { intro x, hinduction x with a a, exact sum.inr a, exact sum.inl a }

@[hott] def flip_flip (x : A ⊎ A) : flip (flip x) = x :=
by { hinduction x, refl, refl }

@[hott] def freegrp_rel : list (A ⊎ A) → list (A ⊎ A) → Type u :=
λ l l', Σ (xs ys : list (A ⊎ A)) (x : A ⊎ A),
  (l = xs ++ x :: flip x :: ys) × (l' = xs ++ ys)

local notation `R ` := freegrp_rel.{u}
local notation `R* ` := trc.{u u} R
local notation `Rs*` := trc (sc R)
local notation c`::`r := trc.snoc.{u u} c r
local notation c`:::`:100d := trc.trans.{u u} _ c d
local notation r`⁺`:1200 := trc.sc.pos.{u u} r
local notation r`⁻`:1200 := trc.sc.neg.{u u} r

@[hott] def freegrp_obj := @quotient unit (λ _ _, A)

@[hott] def freegrp_loops := @eq (@freegrp_obj A)
  (class_of _ ⋆) (class_of _ ⋆)

@[hott] def omega0 : A ⊎ A → (@freegrp_loops A) :=
begin
  intro x, hinduction x with x x,
  { exact eq_of_rel _ x },
  { exact (eq_of_rel _ x)⁻¹ᵖ }
end

@[hott] def omega0_flip_right (x : A ⊎ A) :
  (omega0 x) ⬝ (omega0 (flip x)) = idp :=
by { hinduction x, apply con.right_inv, apply con.left_inv }

@[hott] def omega0_flip_left (x : A ⊎ A) :
  (omega0 (flip x)) ⬝ (omega0 x) = idp :=
by { hinduction x, apply eq.con.left_inv, apply eq.con.right_inv }

@[hott] def omega : quotient (@freegrp_rel A) → (@freegrp_loops A) :=
begin
  intro x, hinduction x with xs xs ys H,
  { hinduction xs with x xs ih, exact idp, exact ih ⬝ omega0 x },
  { hinduction H with xs' H, hinduction H with ys' H,
    hinduction H with x H, hinduction H with eH eH', dsimp,
    rwr eH, clear eH xs, rwr eH', clear eH' ys,
    hinduction xs' with x' xs' ih,
    { transitivity, apply con.assoc,
      transitivity, rwr omega0_flip_left x, refl }, --TODO replace rwr
    { change _ ⬝ _ = _ ⬝ _, apply ap (λ x, x ⬝ omega0 x'),
      exact ih } }
end

-- CvW Lemma 25
@[hott] def freegrp_rel_confluent :
  @is_locally_confluent.{u u} (list (A ⊎ A)) (@freegrp_rel A) :=
begin
  intros l m n r s,
  hinduction r with xs r,  hinduction r with ys r,
  hinduction r with x r, hinduction r with er er',
  rwr er at s, clear er,
  hinduction s with xs' s, hinduction s with ys' s,
  hinduction s with x' s, hinduction s with es es',
  hinduction list_trisection_cons_cons es with mem H H; clear mem,
  { hinduction H with zs H, hinduction H with p1 p2,
    fconstructor, exact xs' ++ zs ++ ys, fconstructor,
    { rwr [er', p1, ←list.append_assoc, ← list.append_assoc],
      apply trc.base, exact ⟨xs', zs ++ ys, x', idp, idp⟩ },
    { rwr [es', p2, list.append_assoc],
      apply trc.base, exact ⟨xs' ++ zs, ys, x, idp, idp⟩ } },
  hinduction H with H H,
  { fconstructor, exact xs' ++ (list.cons x' ys), fconstructor,
    { rwr [er', H.1, ←list.append_assoc], apply trc.refl },
    { rwr [es', ←flip_flip x', ←H.2.1, ←H.2.2], apply trc.refl } },
  hinduction H with H H,
  { fconstructor, exact xs ++ ys, fconstructor,
    { rwr [er'], apply trc.refl },
    { rwr [es', ←H.1, ←H.2.2.2], apply trc.refl } },
  hinduction H with H H,
  { fconstructor, exact xs ++ (list.cons x ys'), fconstructor,
    { rwr [er', H.2.2, H.2.1, flip_flip], apply trc.refl },
    { rwr [es', H.1, ←list.append_assoc], dsimp,
      rwr [←flip_flip x, ←H.2.1, ←H.2.2], apply trc.refl } },
  { hinduction H with zs H, hinduction H with p1 p2,
    fconstructor, exact xs ++ zs ++ ys', fconstructor,
    { rwr [er', p2, list.append_assoc],
      apply trc.base, exact ⟨xs ++ zs, ys', x', idp, idp⟩ },
    { rwr [es', p1, ←list.append_assoc, ←list.append_assoc],
      apply trc.base, exact ⟨xs, zs ++ ys', x, idp, idp⟩ } }
end

@[hott] def freegrp_rel_noetherian :
  hott.acc.well_founded (λ a b, @freegrp_rel A b a) :=
begin
  apply @wf.functor _ (λ a b, @freegrp_rel A b a)
   (λ p q, ulift (nat.lt (list.length p) (list.length q))),
  { intros p q r, fconstructor,
    hinduction r with xs r, hinduction r with ys r,
    hinduction r with x r, hinduction r with e1 e2,
    rwr [e1, e2, list.length_append, list.length_append],
    apply nat.add_lt_add_right,change _ < _ + 2,
    apply nat.lt_add_of_pos_right, fconstructor, fconstructor },
  { apply wf_ulift, apply wf_comp, apply wf_nat_lt }
end

end

end hott