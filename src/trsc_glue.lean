import .trc .quotient_paths_kristina .util

universes u v

hott_theory
namespace hott.quotient

open hott hott.equiv hott.is_equiv hott.eq hott.quotient hott.sigma hott.trunc
open hott.trc hott.is_trunc

section

parameters {A : Type u}
    {R : A → A → Type (max u v)}

local notation `D ` := quotient R
local notation `ι ` := class_of R
local notation `Rs* ` := trc (trc.sc R)

@[hott] def trsc_glue {a b : A} (r : Rs* a b) : ι a = ι b :=
trsc_eq_lift R ι (eq_of_rel R) r

-- CvW Lemma 12
@[hott] def tcsc_glue_surj (a b : A) : is_surjective (@trsc_glue a b) :=
begin
  let P : Π b, ι a = ι b → Type _ 
       := λ b q, trunc -1 (Σ (r : Rs* a b), trsc_glue r = q),
  intro e,
  have Prfl : P a rfl,
  { apply tr, fconstructor, fconstructor, fconstructor },
  have Pcons : ∀ b c (q : ι a = ι b) (s : R b c), P b q ≃ P c (q ⬝ eq_of_rel _ s),
  { intros b c q s, apply equiv_of_is_prop,
    { intro p, hinduction p with p, apply tr, fconstructor,
      exact trc.snoc p.1 (sc.pos s), rwr ←p.2 },
    { intro p, hinduction p with p, apply tr, fconstructor,
      exact trc.snoc p.1 (sc.neg s), apply con_inv_eq_of_eq_con, exact p.2 } },
  have p : ∀ b q, P b q,
  { intros b q, exact @rec' A R a P Prfl Pcons (ι b) q },
  have := p b e, hinduction this with p',
  apply tr, fconstructor, exact p'.1, exact p'.2
end

end

end hott.quotient