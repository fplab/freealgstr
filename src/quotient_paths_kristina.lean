import hott.hit.quotient
import hott.types.sigma
import hott.cubical.square
import .util
universes u v w w'

hott_theory
namespace hott.quotient

open hott hott.equiv hott.is_equiv hott.eq hott.quotient hott.sigma

section 
  parameters {A : Type u}
    {R : A → A → Type v}

local notation `D ` := quotient R
local notation `ι ` := class_of R

parameters {x : A}
    {Q : Π (y : A), ι x = ι y → Type w'}
    (Qrefl : Q x idp)
    (Qcons : Π (y z : A) p (r : R y z), Q y p ≃ Q z (p ⬝ eq_of_rel R r))
include Q Qrefl Qcons

def E' {d e : D} (p : d = e) (F : (ι x = d) → Type w') (G : (ι x = e) → Type w') :
  (F =[p; λ d, (ι x = d) → Type w'] G) ≃ (Π q, F q ≃ G (q ⬝ p)) :=
begin
  hinduction p, transitivity, apply pathover_idp,
  transitivity, apply eq_equiv_homotopy, apply fam_homotopy_equiv_pointwise_equiv
end

def Q' (d : D) : ι x = d → Type w' :=
begin
  hinduction d, exact Q a, apply (E' Qrefl Qcons _ _ _).to_inv,
  intro q, apply Qcons
end

-- CvW Theorem 10
def rec' (d : D) (p : ι x = d) : Q' d p := by { hinduction p, apply Qrefl }

def rec'_refl : rec' _ idp = Qrefl := idp

def rec'_cons_aux (d e : D) (p : ι x = d) (q : d = e) :
  rec' e (p ⬝ q) = E' q (Q' d) (Q' e) (apd Q' q) p (rec' d p) :=
by { hinduction q, hinduction p, refl }

def rec'_cons (y z : A) (p : ι x = ι y) (r : R y z) 
  : rec' (ι z) (p ⬝ eq_of_rel R r) = Qcons y z p r (rec' (ι y) p) :=
begin
  transitivity, apply rec'_cons_aux, apply fn_eq_fn_of_equiv_eq_equiv, 
  apply apd10, apply eq_of_eq_inv, apply hott.quotient.rec_eq_of_rel,
end

end

end hott.quotient
