import hott.hit.quotient
import hott.types.int.basic
import .quotient_paths

universe u

hott_theory
namespace hott

open hott hott.quotient hott.int hott.is_trunc hott.is_equiv

-- assume a bunch of shit about the integers here

@[hott, instance] constant is_equiv_succ : is_equiv (λ (n : ℤ), n + 1)

@[hott] constant int.rec' {C : ℤ → Type u}
  (Czero : C 0)
  (Csucc : Π z, C z → C (z + 1))
  (Cpred Cpred' : Π z, C (z + 1) → C z)
  (Csuccpred : Π z c, Csucc z (Cpred z c) = c)
  (Cpredsucc : Π z c, Cpred' z (Csucc z c) = c)
  : Π (z : ℤ), C z

@[hott] constant int.rec'_zero {C : ℤ → Type u}
  (Czero : C 0)
  (Csucc : Π z, C z → C (z + 1))
  (Cpred Cpred' : Π z, C (z + 1) → C z)
  (Csuccpred : Π z c, Csucc z (Cpred z c) = c)
  (Cpredsucc : Π z c, Cpred' z (Csucc z c) = c)
  : int.rec' Czero Csucc Cpred Cpred' Csuccpred Cpredsucc 0 = Czero

@[hott] constant int.rec'_succ {C : ℤ → Type u}
  (Czero : C 0)
  (Csucc : Π z, C z → C (z + 1))
  (Cpred Cpred' : Π z, C (z + 1) → C z)
  (Csuccpred : Π z c, Csucc z (Cpred z c) = c)
  (Cpredsucc : Π z c, Cpred' z (Csucc z c) = c) (z : ℤ)
  : int.rec' Czero Csucc Cpred Cpred' Csuccpred Cpredsucc (z + 1)
    = Csucc z (int.rec' Czero Csucc Cpred Cpred' Csuccpred Cpredsucc z)

--TODO assume our favorite eliminator for the integers

inductive R : unit → unit → Type
| mk : R () ()

@[hott] def S1 := quotient R
local notation `ι ` := class_of R
@[hott] def base : S1 := ι ()
@[hott] def loop : base = base := eq_of_rel _ R.mk

@[hott] def encode : base = base → ℤ :=
begin
  refine @quotient.path_elim unit R (λ x y, ℤ) _ _ _ () (),
  { intro u, exact 0 },
  { intros u u' u'' r n, exact n + 1 },
  { intros u u' u'' r, apply is_equiv_succ }
end

@[hott] def decode : ℤ → base = base :=
begin
  intro z, refine int.rec' _ _ _ _ _ _ z,
  { refl },                         -- zero case
  { intros k l, exact l ⬝ loop, },  -- succ case
  { intros k l, exact l ⬝ loop⁻¹ }, -- pred case
  { intros k l, exact l ⬝ loop⁻¹ }, -- pred' case
  { intros k l, apply eq.con_eq_of_eq_con_inv, refl },
  { intros k l, apply eq.con_inv_eq_of_eq_con, refl }
end

@[hott] def decode_encode (l : base = base) : decode (encode l) = l :=
begin
  refine @quotient.path_rec _ R (λ x y p, begin
      hinduction x, hinduction y, exact decode (encode p) = p
    end ) _ _ _ _ _ l,
  { intro u, hinduction u, apply int.rec'_zero },
  { intro u, hinduction u, intros u u' p r q, hinduction r, dsimp[encode],
    rwr quotient.path_elim_cons, dsimp[decode], rwr int.rec'_succ,
    apply ap (λ x, x ⬝ loop), exact q },
  { intros u, hinduction u, intros u u' p r, hinduction r, dsimp,
    fapply is_equiv.adjointify,
    { intro q, dsimp[encode] at q, rwr quotient.path_elim_cons at q,
      dsimp[decode] at q, rwr int.rec'_succ at q,
      change decode (encode p) ⬝ loop = p ⬝ loop at q,
      apply hott.eq.cancel_right loop q },
    { dsimp, intro q, sorry },
    { dsimp, intro q, sorry } }
end

@[hott] def S1_Z_equiv : (base = base) ≃ ℤ :=
equiv.MK encode decode _ decode_encode

#check is_prop


end hott