# Coherence via Well-Foundedness

This repository contains the formalization for a draft titles "Coherence via Well-Foundedness -- Taming Set Quotients in Homotopy Type Theory". The files might need a bit more memory to type check than Lean's default 4 GB, but 8 GB should suffice. You will find comments like `-- CvW Lemma 8` in the code referencing the respective lemmas, theorems, and definitions from the paper version. The repository is a Lean Package, relying on the `hott3` library and Lean version 3.4.1.

## src/util.lean

Contains helper lemmas for the remaining files, containing, among others, facts about lists and weakly constant functions.

## src/trc.lean

Constructs and analyses the reflexive-transitive closure `trc R` of a binary relation `R`.

## src/trsc_glue.lean

The definition of and lemmas about glueing along the transitive-symmetric closure.

## src/tc.lean

Facts about the transitive closure `tc R` of a binary relation `R`.

## src/wf.lean

Accessibility and well-foundedness of relations.

## src/confluence.lean

Locally confluent relations and the main result (Theorem 46) of the paper.

## src/cycle_elim.lean

The further main results of our paper: Theorem 14 and Theorem 47.

## src/freegrp.lean

Contains the proof that the reduction relation on the free group on a given type is
locally confluent.

# Path Spaces of Higher Inductive Types

This repository furthermore relies on and for simplicity contains the formalizations for the paper "Path Spaces of Higher Inductive Types in Homotopy Type Theory" by Nicolai Kraus and Jakob von Raumer. This file describes some of the contents and connects them to the respective definitions and propositions of that paper.

## src/util.lean

Contains utility constructions for the other files. Most of them are concerned with path algebra book-keeping.

## src/quotient_paths_categories.lean

Contains the definitions of the two "wild" categories. Their objects are called `CatC_ob` and `CatD_ob`, the morphisms are `CatC_mor` and `CatD_mor` repectively. One difference to the paper is that the category $`\mathcal{D}`$ uses equality instead of equivlence for its third field. `CatD_mor_eq` is a structure used to represent the equalities between morphisms of the category $`\mathcal{D}`$. The initial object in $`\mathcal{D}`$ is definied in by `CatD_init` with the existence of a morphism to any other object $`X`$ constructed in `CatD_init_mor` and its uniqueness shown in `CatD_initiality`.

The equivalence between the two categories is proven in `CatCD_ob_equiv` and `CatCD_mor_equiv` on object and morphism level, respectively. From the equivalence we then analogously to the initiality in $`\mathcal{D}`$ define the initiality in  $`\mathcal{C}`$.

In the section `elim'`, we prove the non-dependent eliminator `path_elim'` including two $`\beta`$-rules as well as the uniquness of the eliminator. The section `rec'` contains constructions for deriving the dependent eliminator `path_rec'` with its $`\beta`$-rules `path_rec'_refl` and `path_rec'_cons`.

The section `elim` wraps univalence around the results of section `elim'` to make the eliminator match the result we present in the paper.

## src/quotient_paths.lean

This file contains roughly the same constructions as `quotient_paths_categories.lean` with the difference that the objects of $`\mathcal{C}`$ are defined using equivalences instead of equalities in its last constructor. Here, we find a way to derive a $`\beta`$-rule which holds definitionally.

## src/pushout_paths.lean

Specializes the previously constructed recurors down to non-dependent and dependent recursors on pushouts, also `path_elim` and `path_rec`.

## src/pushout_embedding.lean

Contains the prove that pushouts preserve embeddings (`preserves_embedding`). The "motive" for the induction is defined in `ap_inr_inv_Q`.
